<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={"get", "post"},
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"category:read", "category:item:get"}}
 *          },
 *          "put"={"security" = "is_granted('ROLE_USER')"},
 *          "delete"={"security" = "is_granted('ROLE_ADMIN')"}
 *      },
 *     collectionOperations={
 *          "get"={},
 *          "post"={"security" = "is_granted('ROLE_USER')"}
 *     },
 *
 *     normalizationContext={"groups"={"category:read"}},
 *     denormalizationContext={"groups"={"category:write"}},
 * )
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"category:read", "category:write", "user:read", "user:write"})
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="categories", cascade={"persist"})
//     * @ORM\JoinColumn(nullable=false)
     * @Groups({"category:read", "category:write"})
     * @Assert\Valid()
     */
    private $owner;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Project", mappedBy="category", cascade={"persist"})
     * @Groups({"category:read", "category:write"})
     * @Assert\Valid()
     */
    private $projects;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Survey", mappedBy="category")
     * @Groups({"category:read", "category:write"})
     * @Assert\Valid()
     */
    private $surveys;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
        $this->surveys = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
            $project->addCategory($this);
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
            $project->removeCategory($this);
        }

        return $this;
    }

    /**
     * @return Collection|Survey[]
     */
    public function getSurveys(): Collection
    {
        return $this->surveys;
    }

    public function addSurvey(Survey $survey): self
    {
        if (!$this->surveys->contains($survey)) {
            $this->surveys[] = $survey;
            $survey->setCategory($this);
        }

        return $this;
    }

    public function removeSurvey(Survey $survey): self
    {
        if ($this->surveys->contains($survey)) {
            $this->surveys->removeElement($survey);
            // set the owning side to null (unless already changed)
            if ($survey->getCategory() === $this) {
                $survey->setCategory(null);
            }
        }

        return $this;
    }
}
